<!DOCTYPE html>
<html>
	<head>
		<title>{% block titulo %}{% endblock %} - Rocci</title>
		<script type="text/javascript" src="{{ res }}/components/jquery/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="{{ res }}/components/bootstrap-4/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="{{ res }}/components/bootstrap-4/css/bootstrap.min.css" />
		{% block cabeza %}{% endblock %}
	</head>
	<body>
		<div id="contenedor">
			{% block contenido %}{% endblock %}
		</div>
	{% block pie %}{% endblock %}
	</body>
</html>