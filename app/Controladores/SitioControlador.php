<?php
	namespace Controlador;
	use Modelo\Cliente as Cliente;
	use Modelo\GrupoCliente as GrupoCliente;
	/**
	* 
	**/
	class SitioControlador extends Controlador {
		
		function __construct() {
			parent::__construct();
		}

		public function index() {
			// Clientes
			$Cliente = new Cliente();
			$clientes = $Cliente->select();
			$clientes_json = json_encode($clientes);
			//Grupo de clientes
			$GrupoCliente = new GrupoCliente();
			$grupos_clientes = $GrupoCliente->select();
			$grupos_clientes_json = json_encode($grupos_clientes);
			
			//Vista
			$this->vista('inicio', [
				'clientes' => $clientes,
				'grupos_clientes' => $grupos_clientes,
				'grupos_clientes_json' => $grupos_clientes_json,
				'clientes_json' => $clientes_json
			]);
		}
		public function editarCliente() {
			$campo = campo();
			$Cliente = new Cliente();
			if(@$campo['id']) {
				$Cliente->condition(['id'=> $campo['id']]);
				$_cliente = $Cliente->get();
			}

			$_cliente['nombre'] = $campo['nombre'];
			$_cliente['apellido'] = $campo['apellido'];
			$_cliente['email'] = $campo['email'];
			$_cliente['grupo_cliente'] = $campo['grupo_cliente'];
			$_cliente['observaciones'] = $campo['observaciones'];

			if(@$campo['id']) {
				$Cliente->condition(['id'=> $campo['id']]);
				$Cliente->update($_cliente);
			} else {
				$Cliente->insert($_cliente);
			}

			echo json_encode($_cliente, JSON_PRETTY_PRINT);
		}
		public function editarGrupoCliente() {
			$campo = campo();
			$GrupoCliente = new GrupoCliente();
			if(@$campo['id']) {
				$GrupoCliente->condition(['id'=> $campo['id']]);
				$_grupo_cliente = $GrupoCliente->get();
			}

			$_grupo_cliente['nombre'] = $campo['nombre'];
			if(@$campo['id']) {
				$GrupoCliente->condition(['id'=> $campo['id']]);
				$GrupoCliente->update($_grupo_cliente);
			} else {
				$GrupoCliente->insert($_grupo_cliente);
			}

			echo json_encode($_grupo_cliente, JSON_PRETTY_PRINT);
		}
		public function listarClientes() {
			$Cliente = new Cliente();
			$clientes = $Cliente->select();
			echo json_encode($clientes, JSON_PRETTY_PRINT);
		}
		public function listarGrupoClientes() {
			$GrupoCliente = new GrupoCliente();
			$grupos_clientes = $GrupoCliente->select();
			echo json_encode($grupos_clientes, JSON_PRETTY_PRINT);
		}
	}