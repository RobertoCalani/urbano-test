<?php

	/*
	|
	|	Configuracion del sitio
	|
	*/

	$sistema = [
		'base'		=>	'',
		'idioma'	=>	'es',
		'desarrollo'=>	true,
	];

	/*
	|
	|	Configuracion de las conexiones
	|
	*/
	$db = [
		'database_type' => 'mysql',
		'database_name' => 'urbano_e',
		'server' => 'localhost',
		'username' => 'root',
		'password' => '',
		'logging' => true
	];